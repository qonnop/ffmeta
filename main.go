package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

const supportedTags = `
album
album_artist
album-sort
artist
artist-sort
author
comment
compilation
composer
copyright
creation_time
date
description
disc
encoded_by
encoder
episode_id
genre
grouping
language
lyrics
network
performer
publisher
rating
show
synopsis
title
title-sort
track
year`

func main() {
	err := run()
	if err != nil {
		fmt.Printf("error: %s\n", err)
		os.Exit(1)
	}
}

type chapter struct {
	Title           string
	OffsetMilliSecs uint64
}

type metadata struct {
	Tags     map[string]string
	Chapters []chapter
}

const usage = `Add metadata and chapter marks to a media file

Usage: ffmeta [-m meta.txt] [-o output.xyz] input.xyz

Create a text file with metadata. Metadata is specified in key-value pairs:
  title     Kitty's Adventures
  author    Mr. Tomcat
  album     Magic Cats
  track     1
  copyright Kitten Media Inc
  comment   Please subscribe to our kitten channel!
  year      2021
Chapter marks consist of the timecode followed by the chapter title:
  00:00 Kitty sleeps
  53:40 Kitty wakes up
  01:02:05.342 Kitty deep sleep
ffmpeg and ffprobe must be in PATH.

Flags:`

func run() error {
	flag.Usage = func() {
		fmt.Println(usage)
		flag.PrintDefaults()
	}
	var infile, metafile, outfile string
	var extractChapters, ignoreMalformed, printProgress bool
	flag.StringVar(&metafile, "m", "", "Metadata file path")
	flag.StringVar(&outfile, "o", "", "Output file path")
	flag.BoolVar(&extractChapters, "x", false, "Extract chapters to individual files")
	flag.BoolVar(&ignoreMalformed, "ignore", false, "Ignore malformed metadata")
	flag.BoolVar(&printProgress, "p", false, "Print ffmpeg progress")

	flag.Parse()

	if flag.NArg() != 1 {
		flag.Usage()
		return nil
	}
	infile = flag.Arg(0)
	if metafile == "" {
		metafile = filePrefix(infile) + ".meta.txt"
	}
	if outfile == "" {
		outfile = infile
	}
	ffmeta1file := filePrefix(infile) + ".ffmetadata1.txt"

	if !executableExistsInPath("ffmpeg") {
		return fmt.Errorf("ffmpeg is not in PATH")
	}
	if !executableExistsInPath("ffprobe") {
		return fmt.Errorf("ffprobe is not in PATH")
	}

	meta, err := readChapters(metafile, ignoreMalformed)
	if err != nil {
		return err
	}

	durationMilliSecs, err := determineMediaDuration(infile)
	if err != nil {
		return err
	}

	if extractChapters && len(meta.Chapters) > 1 {
		for i, c := range meta.Chapters {
			chapterMeta := metadata{}
			chapterMeta.Tags = make(map[string]string)
			for k, v := range meta.Tags {
				chapterMeta.Tags[k] = v
			}
			if _, ok := chapterMeta.Tags["album"]; !ok {
				chapterMeta.Tags["album"] = filePrefix(filepath.Base(infile))
			}
			chapterMeta.Tags["title"] = c.Title
			chapterMeta.Tags["track"] = fmt.Sprintf("%d", i+1)
			chapterMeta.Chapters = nil
			err = writeFFmpegMetadata(&chapterMeta, ffmeta1file, durationMilliSecs)
			if err != nil {
				return err
			}
			var endOffsetMilliSecs uint64
			if i == len(meta.Chapters)-1 {
				endOffsetMilliSecs = durationMilliSecs
			} else {
				endOffsetMilliSecs = meta.Chapters[i+1].OffsetMilliSecs
			}
			err = runFFmpegExtract(infile, ffmeta1file, i, c.Title, c.OffsetMilliSecs, endOffsetMilliSecs, printProgress)
			if err != nil {
				return err
			}

		}
	} else {
		err = writeFFmpegMetadata(meta, ffmeta1file, durationMilliSecs)
		if err != nil {
			return err
		}
		err = runFFmpegCopy(infile, ffmeta1file, outfile, printProgress)
		if err != nil {
			return err
		}
	}

	return deleteFiles(ffmeta1file)
}

func readChapters(metafile string, ignoreMalformed bool) (*metadata, error) {
	f, err := os.Open(metafile)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	mdata := metadata{}
	mdata.Tags = make(map[string]string)
	lineNumber := 0
	malformedLines := make([]int, 0, 10)
	scanner := bufio.NewScanner(f)

	tags := strings.Split(strings.TrimSpace(supportedTags), "\n")
	metaRegex := regexp.MustCompile(`^\s*(` + strings.Join(tags, "|") + `)(\s+(.*))?$`)
	chapterRegex := regexp.MustCompile(`^((\d?\d):)?(\d?\d):(\d\d)(.(\d\d\d))?\s*(.*)$`)

	for scanner.Scan() {
		line := scanner.Text()
		if chapterRegex.MatchString(line) {
			groups := chapterRegex.FindStringSubmatch(line)
			var hour, milliSecond int
			if groups[1] != "" {
				hour, _ = strconv.Atoi(groups[2])
			}
			minute, _ := strconv.Atoi(groups[3])
			second, _ := strconv.Atoi(groups[4])
			if groups[5] != "" {
				milliSecond, _ = strconv.Atoi(groups[6])
			}
			title := strings.TrimSpace(groups[7])
			chapter := chapter{
				Title:           title,
				OffsetMilliSecs: uint64(milliSecond) + uint64(second)*1000 + uint64(minute)*60000 + uint64(hour)*3600000,
			}
			mdata.Chapters = append(mdata.Chapters, chapter)
		} else if metaRegex.MatchString(line) {
			groups := metaRegex.FindStringSubmatch(line)
			key := groups[1]
			if len(groups[2]) > 0 {
				val := strings.TrimSpace(groups[3])
				mdata.Tags[key] = val
			}
		} else if strings.TrimSpace(line) == "" {
			// ignore; empty lines are o.k.
		} else {
			malformedLines = append(malformedLines, lineNumber)
		}
		lineNumber++
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	if len(malformedLines) > 0 && !ignoreMalformed {
		lineNumberStrings := make([]string, 0, len(malformedLines))
		for _, number := range malformedLines {
			lineNumberStrings = append(lineNumberStrings, strconv.Itoa(number+1))
		}
		return nil, fmt.Errorf("%s malformed in line(s) %s", metafile, strings.Join(lineNumberStrings, ", "))
	}

	return &mdata, nil
}

func writeFFmpegMetadata(meta *metadata, metafile string, durationMilliSecs uint64) error {
	f, err := os.Create(metafile)
	if err != nil {
		return err
	}
	defer f.Close()

	// see https://ffmpeg.org/ffmpeg-formats.html#Metadata-1
	escapeSpecialChars := func(str string) string {
		toEscape := []string{`\`, `=`, `;`, `#`, "\n"}
		escaped := str
		for _, toEscape := range toEscape {
			escaped = strings.ReplaceAll(escaped, toEscape, `\`+toEscape)
		}
		return escaped
	}

	f.WriteString(";FFMETADATA1\n")
	for key, val := range meta.Tags {
		if len(val) > 0 {
			f.WriteString(fmt.Sprintf("%s=%s\n", key, escapeSpecialChars(val)))
		}
	}

	for idx, chapter := range meta.Chapters {
		f.WriteString("[CHAPTER]\n")
		f.WriteString("TIMEBASE=1/1000\n")
		f.WriteString(fmt.Sprintf("START=%d\n", chapter.OffsetMilliSecs))
		var end uint64
		if idx == len(meta.Chapters)-1 {
			end = durationMilliSecs
		} else {
			end = meta.Chapters[idx+1].OffsetMilliSecs
		}
		f.WriteString(fmt.Sprintf("END=%d\n", end))
		f.WriteString(fmt.Sprintf("title=%s\n", escapeSpecialChars(chapter.Title)))
	}

	return nil
}

func determineMediaDuration(infile string) (uint64, error) {
	args := []string{"-v", "error", "-show_entries", "format=duration", "-of", "default=noprint_wrappers=1:nokey=1", infile}
	cmd := exec.Command("ffprobe", args...)
	output, err := cmd.Output()
	if err != nil {
		return 0, fmt.Errorf("cannot determine media duration: %s", err)
	}
	outstr := string(output)

	regex := regexp.MustCompile(`(\d+)\.(\d+)`)
	if !regex.MatchString(outstr) {
		return 0, fmt.Errorf("cannot parse ffprobe media duration \"%s\"", outstr)
	}
	groups := regex.FindStringSubmatch(outstr)
	seconds, _ := strconv.Atoi(groups[1])
	microseconds, _ := strconv.Atoi(groups[2])

	return uint64(seconds)*1000 + uint64(microseconds)/1000, nil
}

func runFFmpegExtract(infile, ffmpegMetadataFilename string, chapterIndex int, chapterTitle string, beginOffsetMilliSecs, endOffsetMilliSecs uint64, printProgress bool) error {
	outfile := fmt.Sprintf("%s - %02d - %s%s", filePrefix(infile), chapterIndex+1, replaceReservedPathChars(chapterTitle), filepath.Ext(infile))
	args := []string{"-i", ffmpegMetadataFilename, "-ss", toTimestamp(beginOffsetMilliSecs), "-to", toTimestamp(endOffsetMilliSecs), "-i", infile, "-codec", "copy", "-map_metadata", "0", outfile, "-y"}

	err := execProcess("ffmpeg", printProgress, args...)
	if err != nil {
		return fmt.Errorf("ffmpeg conversion failed: %s", err)
	}

	return nil
}

func runFFmpegCopy(infile, ffmpegMetadataFilename, outfile string, printProgress bool) error {
	override := infile == outfile
	var outArg string
	if override {
		outArg = filePrefix(infile) + ".with-metadata" + filepath.Ext(infile)
	} else {
		outArg = outfile
	}

	args := []string{"-i", ffmpegMetadataFilename, "-i", infile, "-codec", "copy", "-map_metadata", "0", outArg, "-y"}

	err := execProcess("ffmpeg", printProgress, args...)
	if err != nil {
		return fmt.Errorf("ffmpeg conversion failed: %s", err)
	}

	if override {
		err = deleteFiles(infile)
		if err != nil {
			return fmt.Errorf("deleting original file failed: %s", err)
		}
		err = os.Rename(outArg, infile)
		if err != nil {
			return fmt.Errorf("renaming output file failed: %s", err)
		}
	}

	return nil
}

func execProcess(command string, printProgress bool, args ...string) error {
	cmd := exec.Command(command, args...)
	if printProgress {
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
	}
	return cmd.Run()
}

func deleteFiles(filenames ...string) error {
	for _, filename := range filenames {
		err := os.Remove(filename)
		if err != nil {
			return err
		}
	}

	return nil
}

func filePrefix(filename string) string {
	return filename[0 : len(filename)-len(filepath.Ext(filename))]
}

func executableExistsInPath(command string) bool {
	_, err := exec.LookPath(command)
	return err == nil
}

func toTimestamp(milliSeconds uint64) string {
	tmp := milliSeconds
	msecs := tmp % 1000
	tmp /= 1000
	seconds := tmp % 60
	tmp /= 60
	minutes := tmp % 60
	tmp /= 60
	hours := tmp
	return fmt.Sprintf("%02d:%02d:%02d.%03d", hours, minutes, seconds, msecs)
}

func replaceReservedPathChars(str string) string {
	re := regexp.MustCompile(`[\\/\|\?*:"<>\x00-\x1f]`)
	return re.ReplaceAllString(str, " ")
}
