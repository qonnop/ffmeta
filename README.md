# ffmeta

Embed metadata (e.g. title, album, year) and chapter marks in audio or video files and extract chapters to individual files.


## Prerequisites

[ffmpeg](https://ffmpeg.org/download.html) must be installed locally. Both `ffmpeg` and `ffprobe` must be in `PATH`.


## Usage

Imagine an audio or video file, e.g. `CuteKittens.mp4`. Create a text file `CuteKittens.meta.txt` with one line per metadata and a list of chapter marks. All data is optional.

```
title     Kitty's Adventures
author    Mr. Tomcat
album     Magic Cats
track     1
copyright Kitten Media Inc
comment   Please subscribe to our kitten channel!
year      2021

00:00 Kitty sleeps
53:40 Kitty wakes up
01:02:05.342 Kitty deep sleep
```

Chapter marks consist of a timecode and chapter title. The timecode is in format `HH:MM:SS.000` (hours, minutes, seconds, milliseconds). Hours and milliseconds are optional.

`ffmeta` starts an `ffmpeg` subprocess, which embeds the chapter marks and metadata in the video. The file contents (streams) are copied without re-encoding. Metadata already embedded in the input file is removed.

```sh
ffmeta CuteKittens.mp4
```

Use the flag `-x` for extracting all chapters to individual files based on the chapter list in the `.meta.txt` file. All metadata is embedded in the resulting chapter files, but `title` and `track` are overridden with the chapter title and chapter index. `album` is set to the filename if `album` is not specified in the `.meta.txt` file.

```sh
ffmeta -x CuteKittens.mp4
# creates files
# CuteKittens - 01 - Kitty sleeps.mp4
# CuteKittens - 02 - Kitty wakes up.mp4
# CuteKittens - 03 - Kitty deep sleep.mp4
```

`ffmeta` supports all file formats and containers supported by `ffmpeg` (e.g. `mp3`, `mp4`, `mkv`, `webm`, etc.).


## Flags

- `-m` Metadata text file path; defaults to input base file path + `.meta.txt`
- `-o` Output file path
- `-x` Extract chapters to individual files
- `-ignore` Ignore malformed lines
- `-p` Print `ffmpeg` progress
- `-h` Prints usage info and program flags

Flags must be set in front of program arguments.


## Metadata Keys

The [following tags](https://wiki.multimedia.cx/index.php?title=FFmpeg_Metadata) should be supported (depending on the container format):

```
album
album_artist
album-sort
artist
artist-sort
author
comment
compilation
composer
copyright
creation_time
date
description
disc
encoded_by
encoder
episode_id
genre
grouping
language
lyrics
network
performer
publisher
rating
show
synopsis
title
title-sort
track
year
```


## Playback

Many media players support chapter marks, including:

- [VLC media player](https://www.videolan.org/)
- [mpv](https://mpv.io/)


## License

This program is distributed under [MIT](LICENSE) license.
